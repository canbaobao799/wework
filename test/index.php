<?php

namespace QisiWework;

require_once __DIR__ . '/vendor/autoload.php';

// var_dump(User::convert_to_userid('oUkmR4nHvUAFMMT4XDcczALEzN9Q'));


var_dump(Message::send(json_decode('{
   "touser" : "qiudeteng1",
   "msgtype" : "text",
   "agentid" : 1000006,
   "text" : {
       "content" : "你的快递已到，请携带工卡前往邮件中心领取。\n出发前可查看<a href=\"http://work.weixin.qq.com\">邮件中心视频实况</a>，聪明避开排队。"
   },
   "safe":0,
   "enable_id_trans": 0,
   "enable_duplicate_check": 0,
   "duplicate_check_interval": 1800
}', true)));
var_dump(Message::send(json_decode('{
   "touser" : "qiudeteng1",
   "msgtype" : "textcard",
   "agentid" : 1000006,
   "textcard" : {
            "title" : "领奖通知",
            "description" : "<div class=\"gray\">2016年9月26日</div> <div class=\"normal\">恭喜你抽中iPhone 7一台，领奖码：xxxx</div><div class=\"highlight\">请于2016年10月10日前联系行政同事领取</div>",
            "url" : "https://receive.qisiapi.com/",
                        "btntxt":"更多"
   },
   "enable_id_trans": 0,
   "enable_duplicate_check": 0,
   "duplicate_check_interval": 1800
}', true)));
var_dump(Message::send(json_decode('{
   "touser" : "qiudeteng1",
   "msgtype" : "miniprogram_notice",
   "miniprogram_notice" : {
        "appid": "wxa5d138f1b5d04947",
        "page": "/pages/order/orderinfo?order_no=QS00000005000000062021030210000",
        "title": "新订单提醒",
        "description": "'.date("Y-m-d H:i:s").'",
        "emphasis_first_item": true,
        "content_item": [
            {
                "key": "发货时间",
                "value": "'.date("Y-m-d").'"
            },
            {
                "key": "订货门店",
                "value": "相知小炒肉（西二旗店）"
            },
            {
                "key": "订单号",
                "value": "QS00000005000000062021030210000"
            }
        ]
    },
   "enable_id_trans": 0,
   "enable_duplicate_check": 0,
   "duplicate_check_interval": 1800
}', true)));
