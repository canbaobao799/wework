# Qisiapi SDK for PHP


## 概述




## 运行环境
- PHP 5.3+
- cURL extension

## 安装方法

1. 如果您通过composer管理您的项目依赖，可以在你的项目根目录运行：

        $ composer require qisiapi/wework

   或者在你的`composer.json`中声明对QisiWeiWork的依赖：

        "require": {
            "qisiapi/wework": "~1.0"
        }

   然后通过`composer install`安装依赖。composer安装完成后，在您的PHP代码中引入依赖即可：

        require_once __DIR__ . '/vendor/autoload.php';

## 快速使用

    打开Config.php配置一下

### 常用类

| 类名 | 解释 |
|:------------------|:------------------------------------|
|QisiWework\AccessToken::get_access_token() | 获取AccessToken |
|QisiWework\Tools | 一些日常接口例如获取企业微信IP列表|

### 代码示例

```php
<?php


```

## License

- MIT

## 联系我们

- [企思·企业微信组件](https://www.qisiapi.com)
- [企思官方网站](https://www.qisiapi.com)
- [企思官方论坛](https://bbs.qisiapi.com)
- [企思官方文档中心](https://doc.qisiapi.com)
