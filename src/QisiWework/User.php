<?php

/**
 * 企业微信通用方法
 */
declare(strict_types = 1);
namespace QisiWework;

/**
 * 企业微信用户相关操作
 * @author qiude
 *
 */
class User
{

    public static function create()
    {}

    public static function get()
    {}

    public static function update()
    {}

    public static function delete()
    {}

    public static function batchdelete()
    {}

    public static function simplelist()
    {}

    public static function list()
    {}

    /**
     * 将Userid转换成Openid
     *
     * @param string $userid
     */
    public static function convert_to_openid(string $userid)
    {
        $openid = Cache::get(Config::getCacheKey('convert_to_openid' . $userid));
        if ($openid) {
            return $openid;
        }
        $url = 'https://qyapi.weixin.qq.com/cgi-bin/user/convert_to_openid?access_token=' . AccessToken::get_access_token();
        $data = Http::post($url, json_encode([
            'userid' => $userid
        ]));
        $dataArr = json_decode($data, true);
        if ($dataArr['errcode'] !== 0) {
            throw new Exception($dataArr["errmsg"], $dataArr['errcode']);
        }
        Cache::set(Config::getCacheKey('convert_to_openid' . $userid), $dataArr['openid'], 3600);
        return $dataArr['openid'];
    }

    /**
     * 将Openid转换成UserId
     *
     * @param string $openid Openid
     * @throws Exception
     * @return mixed
     */
    public static function convert_to_userid(string $openid)
    {
        $userid = Cache::get(Config::getCacheKey('convert_to_userid' . $openid));
        if ($userid) {
            return $userid;
        }

        $url = 'https://qyapi.weixin.qq.com/cgi-bin/user/convert_to_userid?access_token=' . AccessToken::get_access_token();
        $data = Http::post($url, json_encode([
            'openid' => $openid
        ]));
        $dataArr = json_decode($data, true);
        if ($dataArr['errcode'] !== 0) {
            throw new Exception($dataArr["errmsg"], $dataArr['errcode']);
        }
        Cache::set(Config::getCacheKey('convert_to_userid' . $openid), $dataArr['userid'], 3600);
        return $dataArr['userid'];
    }

    public static function get_active_stat()
    {}

    public static function get_join_qrcode()
    {}
}
