<?php
// +----------------------------------------------------------------------
// | QisiWork
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2019 https://www.qisiapi.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 小九九 <phpjishu@qq.com> <https://www.qisiapi.com>
// +----------------------------------------------------------------------

declare(strict_types=1);

namespace QisiWework;

use QisiWework\Libs\Http;

/**
 * 发送企业微信消息
 * @author qiude
 *
 */
class Message
{
    public static function send($message)
    {
        $url = 'https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=' . AccessToken::get_access_token();
        $data = Http::post($url, json_encode($message));
        $dataArr = json_decode($data, true);
        if ($dataArr['errcode'] !== 0) {
            throw new Exception($dataArr["errmsg"], $dataArr['errcode']);
        }
        return $dataArr;
    }

    public static function update_taskcard($message)
    {
        $url = 'https://qyapi.weixin.qq.com/cgi-bin/message/update_taskcard?access_token=' . AccessToken::get_access_token();
        $data = Http::post($url, json_encode($message));
        $dataArr = json_decode($data, true);
        if ($dataArr['errcode'] !== 0) {
            throw new Exception($dataArr["errmsg"], $dataArr['errcode']);
        }
        return $dataArr;
    }
    private static function get_back_url(){
        
    }
}
