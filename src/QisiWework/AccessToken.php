<?php

/**
 * 企业微信获取access_token相关的方法
 */

declare(strict_types=1);

namespace QisiWework;

use Exception;
use QisiWework\Libs\Http;

class AccessToken
{
    /**
     * 获取access_token
     * @throws Exception
     * @return mixed
     */
    public static function get_access_token()
    {
        $url = 'https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=' . Config::$corpid . '&corpsecret=' . Config::$corpsecret;
        $access_token = Cache::get($url);
        if ($access_token) {
            return $access_token;
        }
        $data = Http::get($url);
        $dataArr = json_decode($data, true);
        if ($dataArr['errcode'] !== 0) {
            throw new Exception($dataArr["errmsg"], $dataArr['errcode']);
        }
        Cache::set($url, $dataArr['access_token'], $dataArr['expires_in'] - 100);
        return $dataArr['access_token'];
    }
}
