<?php

/**
 * 企业微信获取access_token相关的方法
 */

declare(strict_types=1);

namespace QisiWework\Libs;

use \FilesystemIterator;
use QisiWework\Config\Config;

class Cache
{
    public static function  getFileName($name)
    {
        return  md5(Config::$corpid . $name) . '.php';
    }
    public static function set($name, $value, $expire = null): bool
    {
        if (is_null($expire)) {
            $expire = 60;
        }
        $filename = './vendor/qisiapi/wework/cache/' . self::getFileName($name);

        $dir = dirname($filename);

        if (!is_dir($dir)) {
            try {
                mkdir($dir, 0755, true);
            } catch (\Exception $e) {
                // 创建失败
            }
        }
        $data = serialize($value);
        $data   = "<?php\n//" . sprintf('%012d', $expire) . "\n exit();?>\n" . $data;
        $result = file_put_contents($filename, $data);
        if ($result) {
            clearstatcache();
            return true;
        }

        return false;
    }
    /**
     * 判断缓存是否存在
     * @access public
     * @param string $name 缓存变量名
     * @return bool
     */
    public static function has($name): bool
    {
        return self::getRaw($name) !== null;
    }

    /**
     * 读取缓存
     * @access public
     * @param string $name    缓存变量名
     * @param mixed  $default 默认值
     * @return mixed
     */
    public static function get($name, $default = null)
    {

        $raw = self::getRaw($name);

        return is_null($raw) ? $default : unserialize($raw['content']);
    }
    /**
     * 判断文件是否存在后，删除
     * @access private
     * @param string $path
     * @return bool
     */
    private static function unlink(string $path): bool
    {
        try {
            return is_file($path) && unlink($path);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * 删除文件夹
     * @param $dirname
     * @return bool
     */
    private static function rmdir($dirname)
    {
        if (!is_dir($dirname)) {
            return false;
        }

        $items = new FilesystemIterator($dirname);

        foreach ($items as $item) {
            if ($item->isDir() && !$item->isLink()) {
                self::rmdir($item->getPathname());
            } else {
                self::unlink($item->getPathname());
            }
        }

        @rmdir($dirname);

        return true;
    }
    /**
     * 获取缓存数据
     * @param string $name 缓存标识名
     * @return array|null
     */
    protected static function getRaw(string $name)
    {
        $filename = './vendor/qisiapi/wework/cache/' . self::getFileName($name);

        if (!is_file($filename)) {
            return;
        }

        $content = @file_get_contents($filename);

        if (false !== $content) {
            $expire = (int) substr($content, 8, 12);
            if (0 != $expire && time() - $expire > filemtime($filename)) {
                //缓存过期删除缓存文件
                self::unlink($filename);
                return;
            }

            $content = substr($content, 32);

            return is_string($content) ? ['content' => $content, 'expire' => $expire] : null;
        }
    }
}
