<?php

/**
 * 发送HTTP请求
 */

declare(strict_types=1);

namespace QisiWework\Libs;

class Http
{
    /**
     * 发送GET请求，支持https
     *
     * @param string $url
     *            请求地址
     * @param int $timeout
     *            超时时间
     * @return mixed
     */
    public static function get($url, $timeout = 30, $cache = false, $header = [])
    {
        if ($cache) {
            $data = Cache::get(md5($url));
            if ($data !== null) {
                return $data;
            }
        }
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header); // 设置HTTP头信息
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // 这个是重点。
        if (strripos($url, 'https:') !== false) {
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        }
        // 显示输出结果
        $responseText = curl_exec($curl);
        curl_close($curl);
        if ($cache)
            Cache::set(md5($url), $responseText);
        return $responseText;
    }

    /**
     * 发送POST请求
     *
     * @param string $url
     *            请求地址
     * @param array $data
     *            发送数据
     * @param array $header
     *            头数据
     * @param int $timeout
     *            超时时间
     * @param bool $post
     *            请求数据包
     * @return mixed
     */
    public static function post($url, $data, $header = [], $timeout = 60, $post = false)
    {
        // 如果请求的数据为空，则直接发送GET请求
        if (empty($data)) {
            return self::get($url, $timeout);
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header); // 设置HTTP头信息
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1); // 发送一个常规的Post请求
        /* Post提交的数据包 */
        if ($post) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        } else {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        $data = curl_exec($ch);
        @curl_close($ch);
        return $data;
    }
}
